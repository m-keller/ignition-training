## Training Setup installation
<details open>
<summary>Docker Desktop Installation</summary><!-- leave one line empty -->

Requirements:
- Intel i5 6.Gen or newer
- 16Gb Ram
- BIOS-level hardware virtualization support must be enabled in the BIOS settings. For more information, see [Virtualization](https://docs.docker.com/desktop/troubleshoot/topics/#virtualization).
- Windows 10


Install procedure:
- download docker Desktop: https://docs.docker.com/desktop/install/windows-install/
- install as administrator
- if Docker Desktop is not starting up at first start(after installing and rebooting)
  - quit Docker Desktop
  - open powershell as Administrator
  - run command:
    ```
    wsl --update
    ```

</details>

<details open>
<summary>Deploy Ignition Training-Infrastructure</summary><!-- leave one line empty -->

- start Docker Desktop
- Download the Directory [docker-instances](IgnitionTrainingCenter/docker-instances/ "Download")
- unzip directory "docker-instances" to the desktop
- open directory "docker-instances"
- press "shift"+"right Mouse" in this Directory
- click "Open with PowerShell" or "Open in Terminal"
  - ![alt](IgnitionTrainingCenter/media/Infrastructure_openPowershell.png)
- run command: 
  ```
  docker-compose up -d
  ```
- see the running Containers in Docker Desktop
- open a browser and open http://localhost:8088 
 
</details>

---
---

## Papercut (Mail client and SMTP-Server for development)
<details>
<summary>Details</summary>

- to test an email notification on Ignition use following configs in Ignition:
  - Alarm Notification Profiles
    - Hostname: mail
    - port: 25
    - ![alt](IgnitionTrainingCenter/media/MailSetup1.png)
    - add contact info --> mail to a User under Config/Security/User Sources
    - ![alt](IgnitionTrainingCenter/media/MailSetup2.png)
  - create On-Call Roster
  - ![alt](IgnitionTrainingCenter/media/MailSetup3.png)
  - add user to Roster
  - ![alt](IgnitionTrainingCenter/media/MailSetup4.png)
  - open Ignition Designer
  - create new Alarmpipeline
    - add new created On-Call Roster
    - ![alt](IgnitionTrainingCenter/media/MailSetup5.png)
- trigger an alarm and see email under http://localhost:37408
- ![alt](IgnitionTrainingCenter/media/MailSetup6.png)

</details>

---

<!--
## Template
<details>
<summary>Details</summary>

This is a template. Use it do create new chapters
</details>

---

-->
